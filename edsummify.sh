#!/bin/bash

# edsummify.sh
thisdir=$(pwd)

cd /usr/fs/web_d/web/foo/bar/foo/v$1/n$2/xml #fs path

######################################
# functions used in script 			##
######################################
check_article () {
declare -i FOUND=100
declare -i NOT_FOUND=101
declare -a article_found_array=()
# arg=("$@") - multiple articles
index=0
for article in "${article_array[@]}"
	do
	if [ $1 == $article ]; then		#	if the user input matches an element in the article_array,
		article_found_array[index]=$article		#	it gets added to article_found_array
		index=$[$index + 1]		#	increment array index
		#echo $1 found # report that file has been found
		echo $FOUND		#	100 for success, acts like a return statement
	fi
done
	if [ ${#article_found_array[*]} == 0 ]; then		#	if the length of the array is 0, it means no matches were found
		#echo $1 not found # report file not found
		echo $NOT_FOUND		#	101 for not found, again acts like a return statement
	fi
}

add_entity(){

	# $1 is article
	# $2 is edsumm in 130718-1.xml format
	edsumm_ent=$(expr match "$2" '\([0-9]*-[0-9]*\)')		#	strip the '.xml' off the edsumm file name 		
	entity_ref='\&e'$edsumm_ent';'		#	concatenate var to make the entity ref &e130718-1; escape &
	pattern='XML_Article.dtd.>' # pattern to find in xml
	fm_pattern='s/<//fm>/"&"e130711-16;</fm>/'
	
	# now add it to article
	

	sed -i -e "s#"$pattern"#XML_Article.dtd\"\n\[<!ENTITY e"$edsumm_ent" SYSTEM \"edsumm\\/"$edsumm_ent".xml\">\]>#" -e "s#</fm>#"$entity_ref"<\\/fm>#" $1	

	# for reference see: 
	# http://askubuntu.com/questions/76808/how-to-use-variables-in-sed
	# http://stackoverflow.com/questions/7517632/how-do-i-escape-double-and-single-quotes-in-sed-bash
	
	
}

############ functions end #################


###########################################
# setting up of variables and arrays used #
# throughout the script                   #
###########################################



declare -a article_array=($(ls -v))		#	an array with articles in, looks like it's global variable as it works in check_article function
cd edsumm 		#	cd into edsumm folder
declare -a edsumm_array=($(ls -v))		#	an array with the edsumms in
cd ..		# cd back to xml folder

############# variables end ################

read -p "to start adding entities to XML press [ENTER]"

####################################################
# main loop starts 								  ##
####################################################




element_count=${#edsumm_array[@]}
index=0
while [ "$index" -lt "$element_count" ]
	do	
		declare -i not_found=101
		declare -i found=100
		#should be able to skip edsumms if script needs to be run again
		printf "\nEnter file that corresponds to "${edsumm_array[$index]}". \nIf more than one file is associated with "${edsumm_array[$index]}", \nseparate files with a space: \n" 
		#enter 's' to skip this edsumm
		read article1 
		# if [$article1 == 's']; then break fi
		status=$(check_article $article1)
		
		
		while [ "$status" == "$not_found" ]
			do		#	loathe to repeat myself but here goes...
				declare -i not_found=101
				declare -i found=100
				printf "\nFile "$article1" not found, please try again. \nEnter file that corresponds to "${edsumm_array[$index]}".\n" 
				read article1 
				status=$(check_article $article1)
				
				
				
				if [ "$status" -eq "$found" ]; then		#	once article does match break out of this nested while loop
					
					break
					fi 
				
				
			done
		echo "adding edsumm..."
		add_entity $article1 ${edsumm_array[$index]}		#	passing article and edsumm	to add_edsumm function
		echo "edsumm added..."
		((index++))
	done

echo "all edsumms added. "
cd $thisdir #change back to test dir so not calling source from elsewhere